abstract class Motorcycle {
  //abstract class
  var type;
  var series;
  var color;
  var price;

  Motorcycle(type, series, color, price) {
    //class constructor
    this.type = type;
    this.series = series;
    this.color = color;
    this.price = price;

    void introduce() {}
  }
}

class Moving {
  //interface class
  void moveForward() {}
  void moveBackward() {}
}

class Vespa extends Motorcycle implements Moving {
  //inheritance
  Vespa(super.type, super.series, super.color, super.price);

  void introduce() {
    print("Hi there! I'm Vespa $series and my color is $color");
  }

  @override
  void moveForward() {
    print("Move forwards from Vespa ${series} with $color body color");
  }

  @override
  void moveBackward() {
    print("Move backwards from Vespa ${series} with $color body color");
  }
}

class Benelli extends Motorcycle implements Moving {
  //inheritance
  Benelli(super.type, super.series, super.color, super.price);

  void introduce() {
    print("Hi there! I'm Benelli $series and my color is $color");
  }

  @override
  void moveForward() {
    print("Move forwards from Benelli ${series} with $color color");
  }

  @override
  void moveBackward() {
    print("Move backwards from Benelli ${series} with $color color");
  }
}

void main(List<String> arguments) {
  var vespa1 = new Vespa("Scooter", "Sprint LX 125", "White", "93,900");
  var benelli1 = new Benelli("Sport", "TNT 600i", "Black", "275,000");

  vespa1.introduce();
  vespa1.moveForward();
  vespa1.moveBackward();

  benelli1.introduce();
  benelli1.moveForward();
  benelli1.moveBackward();
}
